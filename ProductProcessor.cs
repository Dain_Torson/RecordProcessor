﻿using System;
using System.Linq;
using Ostis.Sctp;
using Ostis.Sctp.Arguments;
using Ostis.Sctp.Commands;
using Ostis.Sctp.CompositeCommands;
using Ostis.Sctp.Responses;
using System.Collections.Generic;

namespace RecordProcessor
{
    class ProductProcessor
    {
        private readonly Identifier rrel_record_field = "rrel_record_field";
        private readonly Identifier rrel_record_first_field = "rrel_record_first_field";
        private readonly Identifier nrel_next_item = "nrel_next_item";
        private readonly Identifier nrel_list_item_value = "nrel_list_item_value";
        private readonly Identifier product_in_invoice_cost = "productInInvoice.cost";
        private readonly Identifier rrel_list_first_item = "rrel_list_first_item";

        SctpClient client;
        public ProductProcessor(string address, int port)
        {
            client = new SctpClient(address, port);
            client.Connect();
        }


        private ScAddress FindFirstField(ScAddress record)
        {
            ScAddress predicate = Commands.FindAddressByIdentifier(client, nrel_list_item_value);

            var template = new ConstructionTemplate(record, ElementType.CommonArc_a,
                ElementType.Link_a, ElementType.PositiveConstantPermanentAccessArc_c, predicate);
            var cmdIterateElements = new IterateElementsCommand(template);
            var rspIterateElements = (IterateElementsResponse)client.Send(cmdIterateElements);

            if (rspIterateElements.Constructions.Count == 1)
            {
                return rspIterateElements.Constructions[0][2];
            }

            return ScAddress.Invalid;
        }

        private ScAddress GetRecordField(ScAddress record, int fieldNumber)
        {

            ScAddress field = FindFirstField(record);
            
            ScAddress predicate = Commands.FindAddressByIdentifier(client, nrel_next_item);

            for (int fieldIdx = 0; fieldIdx < fieldNumber; ++fieldIdx)
            {
                var template = new ConstructionTemplate(field, ElementType.ConstantCommonArc_c,
                    ElementType.Link_a, ElementType.PositiveConstantPermanentAccessArc_c, predicate);
                var cmdIterateElements = new IterateElementsCommand(template);
                var rspIterateElements = (IterateElementsResponse)client.Send(cmdIterateElements);

                if (rspIterateElements.Constructions.Count == 1)
                {
                    field = rspIterateElements.Constructions[0][2];
                }
            }

            return field;
        }

        private void ProcessRecord(ScAddress record)
        {

            ScAddress quantity = GetRecordField(record, 2);
            ScAddress price = GetRecordField(record, 3);

            double quantityValue = LinkContent.ToDouble(Commands.FindLinkContent(client, quantity).GetBytes());
            double priceValue = LinkContent.ToDouble(Commands.FindLinkContent(client, price).GetBytes());
            double costValue = quantityValue * priceValue;

            Console.WriteLine(quantityValue);
            Console.WriteLine(priceValue);
            Console.WriteLine(costValue);

            Tuple<bool, ScAddress> tuple = Commands.CreateLinkWithContent(client, new LinkContent(costValue.ToString()));

            ScAddress cost = tuple.Item2;

            ScAddress invoiceCostField = Commands.FindAddressByIdentifier(client, product_in_invoice_cost);

            Commands.CreateArc(client, ElementType.PositiveConstantPermanentAccessArc_c, invoiceCostField, cost);
            Commands.CreateArc(client, ElementType.ConstantCommonArc_c, GetRecordField(record, 4), cost, nrel_next_item);
        }

        private void ProcessRecordList(ScAddress firstRecord)
        {
            ScAddress record = firstRecord;
            ScAddress predicate = Commands.FindAddressByIdentifier(client, nrel_next_item);

            while (true)
            {
                ProcessRecord(record);

                var template = new ConstructionTemplate(record, ElementType.ConstantCommonArc_c,
                    ElementType.ConstantNode_c, ElementType.PositiveConstantPermanentAccessArc_c, predicate);
                var cmdIterateElements = new IterateElementsCommand(template);
                var rspIterateElements = (IterateElementsResponse)client.Send(cmdIterateElements);

                if (rspIterateElements.Constructions.Count == 1)
                {
                    record = rspIterateElements.Constructions[0][2];
                }
                else
                {
                    break;
                }
            }
        }

        public int CountProceeds(Identifier recordIdentefier)
        {
            ScAddress list = Commands.FindAddressByIdentifier(client, recordIdentefier);
            ScAddress predicate = Commands.FindAddressByIdentifier(client, rrel_list_first_item);

            var template = new ConstructionTemplate(list, ElementType.PositiveConstantPermanentAccessArc_c,
                    ElementType.ConstantNode_c, ElementType.PositiveConstantPermanentAccessArc_c, predicate);
            var cmdIterateElements = new IterateElementsCommand(template);
            var rspIterateElements = (IterateElementsResponse)client.Send(cmdIterateElements);

            ScAddress firstRecord = ScAddress.Invalid;

            if (rspIterateElements.Constructions.Count == 1)
            {
                firstRecord = rspIterateElements.Constructions[0][2];
            }

            if(firstRecord == ScAddress.Invalid)
            {
                return -1;
            }

            ProcessRecordList(firstRecord);

            return 0;
        }

    }
}

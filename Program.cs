﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostis.Sctp;
using Ostis.Sctp.Arguments;


namespace RecordProcessor
{
    class Program
    {
        static void Main(string[] args)
        {

            Identifier identifier = "record_list";
            var processor = new ProductProcessor("192.168.100.5", SctpProtocol.DefaultPortNumber);

            processor.CountProceeds(identifier);

            Console.WriteLine("Done!");
            Console.ReadKey();
        }
    }
}
